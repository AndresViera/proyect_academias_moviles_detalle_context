import React, {useEffect, useState} from 'react';
import Error from './Error';

const initialState={
    nombre:'',
    precio:'',
    categoria:'',
    descripcion:''
}

const errorInit={
    ...initialState
}

const categorias = ['Bebidas', 'Cortes', 'Entradas', 'Postres'];

const Formulario = ({agregarProducto, producto, editar = false, editarProducto }) => {

   
    const [dataForm, setDataForm] = useState(initialState);
    const [errorMessage, setErrorMessage] = useState(errorInit);

    useEffect(()=>{
      if(editar){
        setDataForm({...producto});
      }
    }, []);

    const handleChange = (e) =>{
        //REALIZAMOS UNA COPIA Y MOSTRAMOS CADA ACCION EN EL EVENTO DEL INPUT
        setDataForm({...dataForm, [e.target.name] : e.target.value});
        //AQUI VALIDAMOS QUE SE LLENEN TODOS LOS CAMPOS
        setErrorMessage({...errorMessage, [e.target.name] : ''})
    }

    //AQUI CREAMOS UN METODO PARA VALIDAR LOS CAMPOS
    const isValid = () =>{
        let respuesta = true;
        const localErrors = { ...errorInit };
        for(let key in dataForm){

          if(key !== 'id'){
            if(dataForm[key].trim() === '' || dataForm.length === 0){
              localErrors[key] = 'Campo requerido';
              respuesta = false;
          }
        }

            
        }
        setErrorMessage({ ...localErrors });
        return respuesta;
    }

    
    const handleSubmit =(e)=>{
        e.preventDefault();
        if(isValid()){
          if(editar){
            //llamamos a la funcion de editar
            console.log('vamos a editar el producto');
            editarProducto(dataForm);
          }else{
            //llamamos a la funcion de crear
            console.log('vamos a crear el producto');
            agregarProducto(dataForm);
          }
            
        }
    }

    return (
        <form className='row g-3' onSubmit={handleSubmit}>
        <div className='col-md-6'>
          <label htmlFor='dish' className='form-label'>
            Nombre del plato
          </label>
          <input type='text' className='form-control' name='nombre' onChange={handleChange} value={dataForm.nombre} />
        <Error text={errorMessage.nombre}/>
        </div>
        <div className='col-md-6'>
          <label htmlFor='price' className='form-label'>
            precio
          </label>
          <input type='number' className='form-control' name='precio' onChange={handleChange} value={dataForm.precio} />
          <Error text={errorMessage.precio}/>
        </div>
        <div className='col-6'>
          <label htmlFor='inputAddress' className='form-label'>
            Categoría
          </label>
          <select
            className='form-select'
            name='categoria' onChange={handleChange}
            value={dataForm.categoria}
          >
            <option disabled value=''>
              Selecciona una Categoría
            </option>
                {categorias.map((categoria)=> (<option key={categoria} value={categoria}>{categoria}
            </option>))}
          </select>
          <Error text={errorMessage.categoria}/>
        </div>
        <div className='col-6'>
          <label className='form-label' htmlFor='inputGroupFile01'>
            Imagen
          </label>
          <input
            type='file'
            className='form-control'
            id='inputGroupFile01'
          />
        </div>
        <div className='col-md-12'>
          <label
            htmlFor='exampleFormControlTextarea1'
            className='form-label'
          >
            Descripción
          </label>
          <textarea
            className='form-control'
            name='descripcion'
            rows='3'
            onChange={handleChange} 
            value={dataForm.descripcion}
          ></textarea>
          <Error text={errorMessage.descripcion}/>
        </div>
        <div className='col-12'>
          <button type='submit' className='btn btn-primary'>
            {editar ? 'EDITAR': 'CREAR'}
          </button>
        </div>
      </form>
    );
};

export default Formulario;