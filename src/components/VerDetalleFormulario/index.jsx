import React, {useEffect, useState} from 'react';

const initialState={
    nombre:'',
    precio:'',
    categoria:'',
    descripcion:''
}

const VerDetalleFormulario = ({ producto }) => {

   
    const [dataForm, setDataForm] = useState(initialState);

    useEffect(()=>{
        console.log('aqui traigo al producto');
        setDataForm({...producto});
    }, []);

    return (
      <div className='row g-3'>
      <div className='col-md-6'>
          <label htmlFor='dish' className='form-label'>
            Nombre del plato
          </label>
          <input type='text' className='form-control' name='nombre' value={dataForm.nombre} disabled/>
        </div>
        <div className='col-md-6'>
          <label htmlFor='price' className='form-label'>
            precio
          </label>
          <input type='number' className='form-control' name='precio' value={dataForm.precio} disabled />
          
        </div>
        <div className='col-6'>
          <label htmlFor='inputAddress' className='form-label'>
            Categoría
          </label>
          <input type='text' className='form-control' name='categoria' value={dataForm.categoria} disabled/>
        </div>
        
        <div className='col-md-6'>
          <label
            htmlFor='exampleFormControlTextarea1'
            className='form-label'
          >
            Descripción
          </label>
          <textarea
            className='form-control'
            name='descripcion'
            rows='3' 
            value={dataForm.descripcion}
            disabled
          ></textarea>
        </div>
        <div className='col-12'>
          <button type='submit' className='btn btn-primary'>
            Regresar
          </button>
        </div>
      </div>
  );
};

export default VerDetalleFormulario;