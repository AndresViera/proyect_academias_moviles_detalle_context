import { initializeApp } from "firebase/app";
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyB7CNcXEAm2iVU6LFrZ6EN4lk9BbvdqOGI",
  authDomain: "crud-platos-f0e56.firebaseapp.com",
  projectId: "crud-platos-f0e56",
  storageBucket: "crud-platos-f0e56.appspot.com",
  messagingSenderId: "362939992671",
  appId: "1:362939992671:web:9a551d315e0ee26ed8a9ea",
  measurementId: "G-5WZGFGZQ7X"
};
firebase.initializeApp(firebaseConfig);
//MANEJO DE LA BASE DE DATOS
const db = firebase.firestore();

export {db, firebase};
