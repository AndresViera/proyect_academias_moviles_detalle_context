import React, { useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import VerDetalleFormulario from '../../components/VerDetalleFormulario';
import ProductsContext from '../../context/products/ProductContext';
const VerDetalle = () => {
  const params = useParams();
  const navigate = useNavigate();
  console.log(params);

  const {getByIdProducto, producto} = useContext(ProductsContext);

  useEffect(() =>{
    //llamamos al obtener producto
    getByIdProducto(params.id);
  }, []);

  // useEffect(() =>{
  //   if(verDetalleOK){
  //     navigate('/dashboard');
  //   }
  // }, [verDetalleOK]);

  const tieneProducto = Object.keys(producto).length;

  return (
    <div className='row'>
      <div className='col-12'>
        <h5 className='text-primary text-center py-3'>Ver Detalle Plato</h5>
        <div className='shadow-lg p-3 mb-5 bg-body rounded'>
          {tieneProducto > 0 && <VerDetalleFormulario producto={producto} />}
        </div>
      </div>
    </div>
  );
};

export default VerDetalle;