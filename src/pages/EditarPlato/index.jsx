import React, { useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import Formulario from '../../components/Formulario';
import ProductsContext from '../../context/products/ProductContext';
const EditarPlato = () => {
  const params = useParams();
  const navigate = useNavigate();
  console.log(params);

  const {getByIdProducto, editarProducto, producto, editOK} = useContext(ProductsContext);

  useEffect(() =>{
    //llamamos al obtener producto
    getByIdProducto(params.id);
  }, []);

  useEffect(() =>{
    if(editOK){
      navigate('/dashboard');
    }
  }, [editOK]);

  const tieneProducto = Object.keys(producto).length;

  
  return (
    <div className='row'>
      <div className='col-12'>
        <h5 className='text-primary text-center py-3'>Editar Plato</h5>
        <div className='shadow-lg p-3 mb-5 bg-body rounded'>
          {tieneProducto > 0 && <Formulario producto={producto} editar={true} editarProducto={editarProducto}/>}
        </div>
      </div>
    </div>
  );
};

export default EditarPlato;
