import Table from '../../components/Table';
import { useContext, useEffect } from 'react';
import ProductsContext from '../../context/products/ProductContext';

const Dashboard = () => {

  const {listaProductos, deleteOK, verDetalleOK, obtenerProducto, eliminarProducto, verDetalle} = useContext(ProductsContext);

  useEffect(()=>{
    obtenerProducto()
  },[]);

  useEffect(()=>{
    if(deleteOK){
      obtenerProducto();
    }
  },[deleteOK]);

  useEffect(()=>{
    if(verDetalleOK){
      obtenerProducto();
    }
  },[verDetalleOK]);

  return (
    <>
    <h1>Productos</h1>
      <Table listaProductos={listaProductos} eliminarProducto={eliminarProducto} verDetalle={verDetalle}  />
    </>
  );
};

export default Dashboard;
