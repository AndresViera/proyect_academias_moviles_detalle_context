import React, {useContext, useEffect} from 'react';
import Formulario from '../../components/Formulario';
import ProductsContext from '../../context/products/ProductContext';
import { useNavigate } from 'react-router-dom';

const AgregarPlato = () => {
  const {agregarProducto, addOk} = useContext(ProductsContext);

  let navigate = useNavigate();

  useEffect(()=>{

    if(addOk){
      //redireccion
      navigate('/dashboard');
    }

  }, [addOk])

  return (
    <div className='row'>
      <div className='col-12'>
        <h5 className='text-primary text-center py-3'>Agregar Nuevo Plato</h5>
        <div className='shadow-lg p-3 mb-5 bg-body rounded'>
          <Formulario agregarProducto={agregarProducto} />
        </div>
      </div>
    </div>
  );
};

export default AgregarPlato;
