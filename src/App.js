import Rutas from './routes';
import AuthState from './context/auth/AuthState';
import ProductState from './context/products/ProductState';

function App() {
  return (
    <div className='App'>
     <AuthState>
       <ProductState>
        <Rutas />
       </ProductState>
     </AuthState>
    </div>
  );
}

export default App;
