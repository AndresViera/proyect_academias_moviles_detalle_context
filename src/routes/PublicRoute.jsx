import React from 'react';
import {Navigate} from 'react-router-dom';
import { useContext } from 'react';
import AuthContext from '../context/auth/AuthContext';

const PublicRoute = ({children}) => {

    const {state: {isAuth, consultando}} = useContext(AuthContext);
    if(consultando){
        return "";
    }
    return (
        <div>
            { isAuth ? <Navigate to='/dashboard' /> : children }
        </div>
    );
};

export default PublicRoute;