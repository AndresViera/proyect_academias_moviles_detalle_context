import Header from '../components/Header';
import Siderbard from '../components/Siderbard';
import {Navigate} from 'react-router-dom';
import { useContext } from 'react';
import AuthContext from '../context/auth/AuthContext';

const PrivateRoute = ({ children }) => {

const {state:{isAuth, email, consultando}, cerrarSesion,} = useContext(AuthContext);

if(consultando){
  return "";
}

  return (
    <>
      <section className='d-flex'>
        <Siderbard />
        <div style={{ width: '100%' }}>
          <Header email={email} cerrar={cerrarSesion} />
          <main className='container'>{isAuth ? children : <Navigate to='/'/>}</main>
        </div>
      </section>
    </>
  );
};

export default PrivateRoute;
