import React from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';

import {ListRoute} from './data';

const Router = () => {
    return (
        <BrowserRouter>
            <Routes>
                {ListRoute.map((route, index)=>(
                    <Route key={index} path={route.path} element={route.element} />
                ))}
            </Routes>
        </BrowserRouter>
    );
};

export default Router;