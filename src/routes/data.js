
import PublicRoute from '../routes/PublicRoute';
import PrivateRoute from '../routes/PrivateRoute';

import Login from '../pages/Login';
import Dashboard from '../pages/Dashboard';
import AgregarPlato from '../pages/AgregarPlato';
import EditarPlato from '../pages/EditarPlato';
import VerDetalle from '../pages/VerDetalle';

export const ListRoute=[
    {
        path: '/',
        element:(<PublicRoute><Login /></PublicRoute>)
    },

    {
        path: '/dashboard',
        element:(<PrivateRoute><Dashboard /></PrivateRoute>)
    },

    {
        path: '/agregar-plato',
        element:(<PrivateRoute><AgregarPlato /></PrivateRoute>)
    },

    {
        path: '/editar-plato/:id',
        element:(<PrivateRoute><EditarPlato /></PrivateRoute>)
    },

    {
        path: '/VerDetalle/:id',
        element:(<PrivateRoute><VerDetalle /></PrivateRoute>)
    },

    {
        path: '*',
        element:(
        <div>
            No se encontro la pagina
        </div>)
    },
]